-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2021 at 04:10 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `StartTime` datetime NOT NULL,
  `EndTime` datetime NOT NULL,
  `Contacts` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Resources` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Subject` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`StartTime`, `EndTime`, `Contacts`, `Location`, `Resources`, `Details`, `Subject`) VALUES
('2021-05-07 16:00:00', '2021-05-08 16:00:00', '[]', 'null', '{\"_items\":[],\"m_collectionChanged\":{\"handlers\":[null]},\"m_propertyChanged\":{\"handlers\":[]},\"m_collectionChanging\":{\"handlers\":[]}}', '', 'FINAL DEF'),
('2021-08-10 16:00:00', '2021-08-11 16:00:00', '[]', 'null', '{\"_items\":[],\"m_collectionChanged\":{\"handlers\":[null]},\"m_propertyChanged\":{\"handlers\":[]},\"m_collectionChanging\":{\"handlers\":[]}}', '', 'KEN VILLANUEVA'),
('2021-05-12 16:00:00', '2021-05-12 16:00:00', '[]', 'null', '{\"_items\":[],\"m_collectionChanged\":{\"handlers\":[null]},\"m_propertyChanged\":{\"handlers\":[]},\"m_collectionChanging\":{\"handlers\":[]}}', '', 'aSASQSQ'),
('2021-05-12 16:00:00', '2021-05-13 16:00:00', '[]', 'null', '{\"_items\":[],\"m_collectionChanged\":{\"handlers\":[null]},\"m_propertyChanged\":{\"handlers\":[]},\"m_collectionChanging\":{\"handlers\":[]}}', '', 'badato'),
('2021-05-18 16:00:00', '2021-05-19 16:00:00', '[]', 'null', '{\"_items\":[],\"m_collectionChanged\":{\"handlers\":[null]},\"m_propertyChanged\":{\"handlers\":[]},\"m_collectionChanging\":{\"handlers\":[]}}', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
