-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2021 at 04:10 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inmates`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `contact`) VALUES
(1, 'Randolf Jay', 'jay@gmail.com', '09271652211'),
(2, 'jay tiposo', 'jay@gmail.com', '09271122334'),
(3, 'adrian villanueva', 'bakla@gmail.com', '12345333'),
(4, 'Jennilyn Enriquez', 'jen@yahoo.com', '1233332222'),
(5, 'Ken Laurente', 'ken@gmail.com', '2232232321312'),
(6, 'Justine Villanueva', 'just@yahoo.com', '276222222'),
(7, 'Jehvy Econar', 'Jehv@gmail.com', '275256462552'),
(8, 'Riam Jayden Tiposo', 'riam@yahoo.com', '72677263'),
(9, 'Ken Villanueva', 'ken@yahoo.com', '09053205087'),
(10, 'Thor Dimalanta-edited', 'thor@gmail.com', '09218416952'),
(11, 'fernando poe', 'tsfags@gmail.com', '2312242'),
(12, 'digong ', 'ascsac@yahoo.com', '31212312'),
(13, 'raizen james tiposo', 'sjcbjb@gmail.com', '223341'),
(14, 'Raine Jasper Tiposo', 'asjgch@yahoo.com', '242132132'),
(15, 'Ralf Jiro Tiposo', 'wdgw@gmail.com', '212321323'),
(16, 'Riam Jayden Tiposo', 'wqfhwf@gmail.com', '23132321'),
(17, 'riri sc', 'riri@gmail.com', '2132132123'),
(18, 'lowkey acs', 'sfsd@yahii.com', '23124214'),
(19, 'agsdhg', '232@g.com', '2412321'),
(20, 'wqfwqw', 'qwdwq@ege.com', '12414'),
(21, 'wqewqeqw', '213123@d.ffd', '123213'),
(22, 'wdwdwqdwq', '12eq@efef.com', '21321321'),
(23, 'Ron asfwaqfq', '2321wdqwd@yahoo.com', '82646753353'),
(24, 'badato', 'ssas@yahoo.com', '313324124');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
