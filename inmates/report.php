<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width", initial-scale=1">
    <title>REPORT</title>
	
    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Materials+Icons">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	
	</style>
  </head>
  <body onload="print()">
	<div class="container">
		<center>
			<h3 style="margin-top: 30px">Inmates Contact List</h3>
			<hr>
		</center>
		<table id="ready" class="table table-striped table-bordered" style="width: 100%;">
			<thead>
				<tr>
					<th>ID</th>
					<th>Inmate Name</th>
					<th>EMAIL</th>
					<th>Relatives Number</th>
				</tr>
			</thead>
			<tbody>
				<?php
					include 'connection.php';
					$get_list = mysqli_query($con, "SELECT * FROM contacts");
					
					while($row = mysqli_fetch_array($get_list)){
					
				?>
					<tr>
						<td><?php echo $row['id']?></td>
						<td><?php echo $row['name']?></td>
						<td><?php echo $row['email']?></td>
						<td><?php echo $row['contact']?></td>
					</tr>
					
				
				<?php } ?>
			</tbody>
		</table>	
	</div>
	
    

  </body>
</html>