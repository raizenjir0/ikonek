<?php
    // Connect to the database
    include_once "connection.php";
	$num_per_page = 04;
	
	
	if(isset($_GET["page"]))
	{
		$page=$_GET["page"];
	}
	else
	{
		$page=1;
	}	
	
	$start_from=($page-1)*04;
	
	$query = "SELECT * FROM contacts limit $start_from,$num_per_page";
	$result = mysqli_query($con,$query);
	
	
	
    // Delete Table data
    if (isset($_GET["del"])) {
        $id = preg_replace('/\D/', '', $_GET["del"]); //Accept numbers only
        if ($stmt = $con->prepare("DELETE FROM `contacts` WHERE `id`=?")) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->close();
            $msg = '<div class="msg msg-delete">Contact details deleted successfully.</div>';
        } else {
            die('prepare() failed: ' . htmlspecialchars($con->error));
        }
    }

    // Display Table data
    $tabledata = "";
    $sqlsearch = "";
    if (isset($_POST["btnSearch"])) {
        $keywords = $con->real_escape_string($_POST["txtSearch"]);
        $searchTerms = explode(' ', $keywords);
        $searchTermBits = array();
        foreach ($searchTerms as $key => &$term) {
            $term = trim($term);
            $searchTermBits[] = " `name` LIKE '%$term%' OR `email` LIKE '%$term%' OR `contact` LIKE '%$term%'";
        }
        $sqlsearch = " WHERE " . implode(' AND ', $searchTermBits);
    }

    if ($stmt = $con->prepare("SELECT * FROM `contacts` $sqlsearch")) {
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $tabledata .= '<tr>
                                <td>'.$row["name"].'</td>
                                <td>'.$row["email"].'</td>
                                <td>'.$row["contact"].'</td>
                                <td>
                                    <a href="update.php?id='.$row["id"].'" class="btnAction btnUpdate" title="Update contact details">&#9998;</a>
                                    <a href="index.php?del='.$row["id"].'" class="btnAction btnDelete" title="Delete contact details">&#10006;</a>
                                </td>
                            </tr>';
            }
        } else {
            $tabledata= '<tr><td colspan="4" style="text-align: center; padding:30px 0;">Nothing to display</td></tr>';
        }

        $stmt->close();
    } else {
        die('prepare() failed: ' . htmlspecialchars($con->error));
    }

    // Close database connection
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INMATES MANAGEMENT CONTACT</title>
    <link rel="stylesheet" href="style1234.css">
	
</head>
<body>
    <?php if(isset($msg)){ echo $msg; }?>
	
    <main class="container">
        <div class="wrapper"> 
            <h1>I-KONEK: INMATES MANAGEMENT CONTACT</h1>
            
        </div>
        <div class="wrapper">
            <a href="create.php" class="btnCreate" title="Create new contact">REGISTER</a>
			
        </div>
        <div class="wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <input type="text" name="txtSearch" value="<?php if(isset($keywords)){ echo $keywords; }?>" title="Input keywords here" required>
                <button type="submit" name="btnSearch" class="btnSearch" title="Search keywords">Search</button>
                <a href="http://localhost/logg/DASHBB.html#" class="btnReset" title="Reset search">HOME</a>
            </form>
        </div>
        <div class="wrapper">
            <table>
                <thead>
                    <tr>
                        <th>Inmates Name</th>
                        <th>Email</th>
                        <th>Relative Contact</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        echo $tabledata;
                    ?>
					<?php
					
					
					?>
                </tbody>
				<?php
				$query = "SELECT * FROM contacts";
				$pr_result = mysqli_query($con,$query);
				$totalrecord = mysqli_num_rows($pr_result);
				$totalpages = ceil($totalrecord/$num_per_page);
				
		
				for($i=1;$i<=$totalpages;$i++)
				{
					echo "<a href='indexx.php?page=".$i."'class='btn btn-succes'>$i</a>";
					
				}
			
			?>
            </table>
			
			
        </div>
    </main>
	
</body>
</html>