<?php
session_start();
if (!isset($_SESSION['name']) && !isset($_SESSION['id'])) {
    header('location: ../../index.php');
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>IKONEK HOMEPAGE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="style1234.css"> -->
    <link href='../../assets/packages/fullcalendar/main.css' rel='stylesheet' />
</head>

<body>

    <div class="header">
        <h3>I-KONEK</h3>
        <a href="../../destroy_session.php" class="btn btn-link">
            Sign out
            &nbsp;
            <i class="fa fa-sign-out-alt"></i>
        </a>
    </div>

    <div class="wrapper">
        <div class="sidebar">
            <div class="brand">
                <img src="../../assets/images/QC logo.png" class="profile_image" alt="">
                <h4>BJMP QC</h4>
            </div>

            <a href="../dashboard/">
                <i class="fa fa-home"></i>
                <span>Home Page</span>
            </a>

            <a href="../inmates/">
                <i class="fa fa-users"></i>
                <span>Inmates</span>
            </a>

            <a href="../schedules/">
                <i class="fa fa-calendar"></i>
                <span>Schedules</span>
            </a>

            <a href="../sms">
                <i class="fas fa-sms"></i>
                <span>SMS Notifications</span>
            </a>

            <?php if ($_SESSION["role"] === 'admin') { ?>
                <a href="../videocall/">
                    <i class="fa fa-video-camera"></i>
                    <span>Video Call</span>
                </a>

                <a href="../reports" target="_blank">
                    <i class="fas fa-info-circle"></i>
                    <span>Reports</span>
                </a>

                <a href="../users">
                    <i class="fas fa-cog"></i>
                    <span>User Setup</span>
                </a>
            <?php } ?>



        </div>


        <div class="content">