<?php include('../layouts/header.php') ?>
<h5>Inmate Schedules</h5>
<hr>
<div id='calendar'></div>

<?php include('../layouts/footer.php') ?>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var date = new Date()
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear()

        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            selectable: true,
            editable: true,
            events: "fetch_event.php",
            eventClick: function (event) {
                console.log(event.event.id)
            var deleteMsg = confirm("Do you really want to delete?" + event.event.id);
            
            if (deleteMsg) {
                $.ajax({
                    type: "POST",
                    url: "delete-event.php",
                    data: "&id=" + event.event.id,
                    success: function (response) {
                        if(parseInt(response) > 0) {
                            console.log(event);
                            // $('#calendar').fullCalendar('removeEvents', event.event.id);
                            // displayMessage("Deleted Successfully");
                            window.location.reload();
                        }
                    }
                });
            }
        },
        });
        calendar.render();
    });
</script>
<!-- events: [{
title: 'All Day Event',
start: new Date(y, m, 1),
backgroundColor: '#f56954', //red
borderColor: '#f56954', //red
allDay: true
},
{
title: 'Long Event',
start: new Date(y, m, d - 5),
end: new Date(y, m, d - 2),
backgroundColor: '#f39c12', //yellow
borderColor: '#f39c12' //yellow
},
{
title: 'Meeting',
start: new Date(y, m, d, 10, 30),
end: new Date(y, m, d, 11, 00),
allDay: false,
backgroundColor: '#0073b7', //Blue
borderColor: '#0073b7' //Blue
},
{
title: 'Lunch',
start: new Date(y, m, d, 11, 00),
end: new Date(y, m, d, 14, 0),
allDay: false,
backgroundColor: '#00c0ef', //Info (aqua)
borderColor: '#00c0ef' //Info (aqua)
},

{
title: 'Birthday Party',
start: new Date(y, m, d + 1, 19, 0),
end: new Date(y, m, d + 1, 22, 30),
allDay: false,
backgroundColor: '#00a65a', //Success (green)
borderColor: '#00a65a' //Success (green)
},
{
title: 'Click for Google',
start: new Date(y, m, 28),
end: new Date(y, m, 29),
url: 'https://www.google.com/',
backgroundColor: '#3c8dbc', //Primary (light-blue)
borderColor: '#3c8dbc' //Primary (light-blue)
}
], -->