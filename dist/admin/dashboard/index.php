<?php

require_once '../../conn.php';
date_default_timezone_set("Asia/Manila");
$currentDate1 = date('Y-m-d');

function countData($conn, $from, $extend)
{
    $count = 0;
    if ($stmt = $conn->prepare("SELECT count(id) FROM $from $extend")) {
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->free_result();
        $stmt->close();
    } else {
        die('prepare() failed: ' . htmlspecialchars($conn->error));
    }

    return $count;
}
?>

<?php include('../layouts/header.php') ?>
<h5>Dashboard</h5>
<hr>

<div class="alert alert-success alert-dismissible" role="alert">
    <h6 class="h6">Welcome <?php echo $_SESSION["name"] ?>!</h6>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="box box-success">
            <h2><?php echo countData($conn, 'inmates', ' Where is_good_moral = 1') ?></h2>
            <h6>Good Moral</h6>
            <i class="fas fa-heart"></i>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <h2><?php echo countData($conn, 'inmates', '') ?></h2>
            <h6>Inmates</h6>
            <i class="fas fa-users"></i>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-warning">
            <h2><?php echo countData($conn, 'users', '') ?></h2>
            <h6>Users</h6>
            <i class="fas fa-user"></i>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-info">
            <h2><?php echo countData($conn, 'schedules', "Where start LIKE '%$currentDate1%'") ?></h2>
            <h6>Scheduled this day</h6>
            <i class="fas fa-calendar"></i>
        </div>
    </div>

</div>

<?php include('../layouts/footer.php') ?>