<?php include('../layouts/header.php') ?>
<h5> SMS Notifications </h5>
<hr>
<div class="number">
	<form action="text.php" method="post">
		<div class="from-group">
			<label for="">Mobile Number</label>
			<input type="text" name="mobile_number" size="20" class="form-control">
		</div>
		<br>
		<div class="from-group">
			<label for="">Messages</label>
			<textarea cols=25 rows=5 name="messages" class="form-control"></textarea>
		</div>
		<br>
		<input type="submit" name="action" value="Send SMS" title="Click here to send SMS." class="btn btn-success">
	</form>
</div>
<?php include('../layouts/footer.php') ?>