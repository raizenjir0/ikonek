<?php
// Delete Table data
include_once "../../conn.php";
if (isset($_POST["btnSave"])) {
    // Connect to the database

    $name    = $conn->real_escape_string($_POST["name"]);
    $username   = $conn->real_escape_string($_POST["username"]);
    $password = $conn->real_escape_string(md5($_POST["password"]));
    $role = 'user';

    if ($stmt = $conn->prepare("INSERT INTO `users`(`name`, `username`, `password`, `role`) VALUES (?, ?, ?, ?)")) {
        $stmt->bind_param("ssss", $name, $username, $password, $role);
        $stmt->execute();
        $stmt->close();
        $msg = '<div class="msg msg-create">Contact details saved successfully.</div>';
    } else {
        $msg = '<div class="msg">Prepare() failed: ' . htmlspecialchars($conn->error) . '</div>';
    }

    // Close database connection
    $conn->close();
}
?>
<?php include('../layouts/header.php') ?>

<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<div class="title create">
    <h5>New User</h5>
    <hr>
</div>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="frmCreate">
    <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="name" placeholder="Ex. John Doe" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Username</label>
        <input type="text" name="username" placeholder="Ex. johndoe" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Password</label>
        <input type="password" name="password" required class="form-control">
    </div>

    <!-- <input type="email" name="txtEmail" placeholder="Email" required>
    <input type="number" min="0" name="txtContact" placeholder="Contact Number" required> -->
    <div class="btnWrapper">
        <button type="submit" name="btnSave" title="Save contact details" class="btn btn-success">SAVE</button>
        <a href="index.php" title="Return back to homepage" class="btn btn-info">BACK</a>
    </div>

    <?php include('../layouts/footer.php') ?>