<?php
// Connect to the database
include_once "../../conn.php";

// Get password details
if (isset($_GET["id"])) {
    $id = preg_replace('/\D/', '', $_GET["id"]); //Accept numbers only
} else {
    header("Location: indexx.php?p=update&err=no_id");
}

// Update password details
if (isset($_POST["btnUpdate"])) {
    $name    = $conn->real_escape_string($_POST["name"]);
    $username   = $conn->real_escape_string($_POST["username"]);
    // $password = $conn->real_escape_string($_POST["password"]);
    // $is_good_moral = $conn->real_escape_string($_POST["textIsGoodMoral"]);

    if ($stmt = $conn->prepare("UPDATE `users` SET `name`=?, `username`=? WHERE `id`=?")) {
        $stmt->bind_param("ssi", $name, $username, $id);
        $stmt->execute();
        $stmt->close();
        $msg = '<div class="msg msg-update">User details updated successfully.</div>';
    } else {
        $msg = '<div class="msg">Prepare() failed: ' . htmlspecialchars($conn->error) . '</div>';
    }
}


if ($stmt = $conn->prepare("SELECT `name`, `username` FROM `users` WHERE `id`=? LIMIT 1")) {
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->bind_result($name, $username);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
} else {
    die('prepare() failed: ' . htmlspecialchars($conn->error));
}

// Close database connection
$conn->close();
?>
<?php include('../layouts/header.php') ?>
<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
<div class="title update">
    <h5>Update password</h5>
    <hr>
</div>
<form action="<?= $_SERVER['PHP_SELF'] . "?id=" . $id; ?>" method="post" class="frmUpdate">
    <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="name" placeholder="John Doe" value="<?php echo $name; ?>" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Username</label>
        <input type="username" name="username" placeholder="johndoe" value="<?php echo $username; ?>" required class="form-control">
    </div>

    <div class="btnWrapper">
        <button type="submit" name="btnUpdate" class="btn btn-primary" title="Update password details">UPDATE</button>
        <a href="index.php" class="btn btn-info" title="Return back to homepage">BACK</a>
    </div>
</form>
<?php include('../layouts/footer.php') ?>