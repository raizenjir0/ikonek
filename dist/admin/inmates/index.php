<?php
// Connect to the database
include_once "../../conn.php";
$num_per_page = 5;

if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}

$start_from = ($page - 1) * $num_per_page;

$query = "SELECT * FROM inmates limit $start_from, $num_per_page";
$result = mysqli_query($conn, $query);



// Delete Table data
if (isset($_GET["del"])) {
    $id = preg_replace('/\D/', '', $_GET["del"]); //Accept numbers only
    if ($stmt = $conn->prepare("DELETE FROM `inmates` WHERE `id`=?")) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
        $msg = '<div class="msg msg-delete">Contact details deleted successfully.</div>';
    } else {
        die('prepare() failed: ' . htmlspecialchars($connn->error));
    }
}

// Display Table data
$tabledata = "";
$sqlsearch = "";
if (isset($_POST["btnSearch"])) {
    $keywords = $conn->real_escape_string($_POST["txtSearch"]);
    $searchTerms = explode(' ', $keywords);
    $searchTermBits = array();
    foreach ($searchTerms as $key => &$term) {
        $term = trim($term);
        $searchTermBits[] = " `name` LIKE '%$term%' OR `email` LIKE '%$term%' OR `contact` LIKE '%$term%'";
    }
    $sqlsearch = " WHERE " . implode(' AND ', $searchTermBits);
}



// Close database connection

?>
<?php include('../layouts/header.php') ?>
<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<div class="d-flex">
    <h5>Inmates / Contacts</h5>
    <a href="create.php" class="btn btn-info ml-auto btn-sm" title="Create new contact">Create new contact</a>
</div>
<hr>
<br>
<div class="">
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group d-flex">
            <input type="text" name="txtSearch" value="<?php if (isset($keywords)) {
                                                            echo $keywords;
                                                        } ?>" title="Input keywords here" required class="form-control">
            <button type="submit" name="btnSearch" class="btn btn-success ml-2" title="Search keywords">Search</button>
        </div>

    </form>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Inmates Name</th>
                <th>Email</th>
                <th>Relative Contact</th>
                <th>Good Moral</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($stmt = $conn->prepare("SELECT * FROM `inmates` $sqlsearch LIMIT $start_from, $num_per_page")) {
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        if ($_SESSION["role"] === 'admin') {
                            $actionButtons = '
                                <a href="update.php?id=' . $row["id"] . '" class="btn btn-light btn-sm" title="Update contact details">
                                        <i class="far fa-edit"></i>
                                </a>
                                <a href="index.php?del=' . $row["id"] . '" class="btn btn-light btn-sm" title="Delete contact details">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            ';
                        } else {
                            $actionButtons = '';
                        }
                        if($row["is_good_moral"] === 1) {
                            $good_moral_badge = '<span class="badge badge-success">Good Moral</span>';
                        }else{
                            $good_moral_badge = '<span class="badge badge-secondary">None</span>';
                        }
                        

                        $tabledata .= '<tr>
                                <td>' . $row["name"] . '</td>
                                <td>' . $row["email"] . '</td>
                                <td>' . $row["contact"] . '</td>
                                <td>' . $good_moral_badge . '</td>
                                <td>
                                    <a href="schedule.php?id=' . $row["id"] . '" class="btn btn-light btn-sm" title="Update contact details">
                                    <i class="far fa-calendar"></i>
                                </a>
                                    ' . $actionButtons . '
                                </td>
                            </tr>';
                    }
                } else {
                    $tabledata = '<tr><td colspan="4" style="text-align: center; padding:30px 0;">Nothing to display</td></tr>';
                }

                $stmt->close();
            } else {
                die('prepare() failed: ' . htmlspecialchars($conn->error));
            }
            echo $tabledata;
            ?>
        </tbody>


        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <!-- <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li> -->

                <?php
                $query = "SELECT * FROM inmates";
                $pr_result = mysqli_query($conn, $query);
                $totalrecord = mysqli_num_rows($pr_result);
                $totalpages = ceil($totalrecord / $num_per_page);


                for ($i = 1; $i <= $totalpages; $i++) {
                    echo "<li class='page-item'><a class='page-link' href='index.php?page=" . $i . "'>$i</a></li>";
                }

                ?>
                <!-- <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li> -->
            </ul>
        </nav>
    </table>


</div>


<?php include('../layouts/footer.php') ?>