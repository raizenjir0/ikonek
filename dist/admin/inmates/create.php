<?php
// Delete Table data
include_once "../../conn.php";
if (isset($_POST["btnSave"])) {
    // Connect to the database

    $name    = $conn->real_escape_string($_POST["txtName"]);
    $email   = $conn->real_escape_string($_POST["txtEmail"]);
    $contact = $conn->real_escape_string($_POST["txtContact"]);
    $goodMoral = $conn->real_escape_string($_POST["textIsGoodMoral"]);

    if ($stmt = $conn->prepare("INSERT INTO `inmates`(`name`, `email`, `contact`, `is_good_moral`) VALUES (?, ?, ?, ?)")) {
        $stmt->bind_param("ssss", $name, $email, $contact, $goodMoral);
        $stmt->execute();
        $stmt->close();
        $msg = '<div class="msg msg-create">Contact details saved successfully.</div>';
    } else {
        $msg = '<div class="msg">Prepare() failed: ' . htmlspecialchars($conn->error) . '</div>';
    }

    // Close database connection
    $conn->close();
}
?>
<?php include('../layouts/header.php') ?>

<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<div class="title create">
    <h5>Create New Contact</h5>
    <hr>
</div>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="frmCreate">
    <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="txtName" placeholder="Ex. John Doe" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input type="email" name="txtEmail" placeholder="Ex. johndoe@example.com" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Contact</label>
        <input type="number" name="txtContact" placeholder="Ex. 09239239333" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Inmates Good Moral</label>
        <select name="textIsGoodMoral" id="" class="form-control">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </div>

    <!-- <input type="email" name="txtEmail" placeholder="Email" required>
    <input type="number" min="0" name="txtContact" placeholder="Contact Number" required> -->
    <div class="btnWrapper">
        <button type="submit" name="btnSave" title="Save contact details" class="btn btn-success">SAVE</button>
        <a href="index.php" title="Return back to homepage" class="btn btn-info">BACK</a>
    </div>

    <?php include('../layouts/footer.php') ?>