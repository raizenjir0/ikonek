<?php
// Connect to the database
include_once "../../conn.php";

// Get contact details
if (isset($_GET["id"])) {
    $id = preg_replace('/\D/', '', $_GET["id"]); //Accept numbers only
} else {
    header("Location: indexx.php?p=update&err=no_id");
}




date_default_timezone_set("Asia/Manila");
$currentDate1 = date('Y-m-d');
$currentDate = date('Y-m-d', strtotime($currentDate1. ' + 1 days'));
// Update contact details

if ($stmt = $conn->prepare("SELECT `name`, `contact`, `is_good_moral` FROM `inmates` WHERE `id`=? LIMIT 1")) {
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->bind_result($name, $contact, $isGoodMoral);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
} else {
    die('prepare() failed: ' . htmlspecialchars($conn->error));
}

if ($stmt = $conn->prepare("SELECT title, Max(start), MAX(end) as end FROM `schedules` Where end LIKE '%$currentDate%' LIMIT 1")) {
    // $stmt->bind_param("i", $id);

    $stmt->execute();
    $stmt->bind_result($title, $start, $end);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();

    if (empty($title)) {
        $start = new DateTime($currentDate . ' 08:00:00');
        $end = new DateTime($currentDate . ' 08:30:00');
        $start = $start->format('Y-m-d H:i');
        $end = $end->format('Y-m-d H:i');
    } else {
        $minutes_to_add = 30;
        $start = new DateTime($start);
        $end = new DateTime($end);
        $start->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        $end->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        $start = $start->format('Y-m-d H:i');
        $end = $end->format('Y-m-d H:i');
    }
} else {
    die('prepare() failed: ' . htmlspecialchars($conn->error));
}


// Close database connection
$conn->close();
?>
<?php include('../layouts/header.php') ?>
<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
<div class="title update">
    <h5>Smart Scheduler</h5>
    <hr>
</div>
<form action="text.php" method="post">
    <div class="form-group">
        <label for="">Schedule Title</label>
        <input type="text" name="title" placeholder="Name" value="<?php echo $name; ?>" required class="form-control" >
    </div>
    <div class="form-group">
        <label for="">This number will get the message for the schedule</label>
        <input type="text" name="contact" placeholder="Name" value="<?php echo $contact; ?>" required class="form-control" >
    </div>
    <br>
    <input type="hidden" id="manual" name="manual" value="gen_date">
    <div class="form-group" id="form_generated_date">
        <h6>Generated Date</h6>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <label for="">Start Session</label>
                <input type="datetime-local" name="start" placeholder="Name" value="<?php echo date('Y-m-d\TH:i:s', strtotime($start)); ?>" required class="form-control">
            </div>

            <div class="col-md-6">
                <label for="">End Session</label>
                <input type="datetime-local" name="end" placeholder="Name" value="<?php echo date('Y-m-d\TH:i:s', strtotime($end)); ?>" required class="form-control">
            </div>
        </div>
    </div>
    <br>
    

        <div class="form-group" style="display: none;" id="form_manual_sched">
        <h6>Inmate Privilege (Good Moral Inmates)</h6>
        <hr>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Start Session</label>
                    <input type="datetime-local" name="manual_start" placeholder="Name" value="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="">End Session</label>
                    <input type="datetime-local" name="manual_end" placeholder="Name" value="" class="form-control">
                </div>
            </div>
        </div>
    <button type="button" class="btn btn-info mb-3" id="btn_manual_sched">Manual Schedule</button>   
    <br>
    <div class="btnWrapper">
        <button type="submit" name="btn_set_schedule" class="btn btn-primary" title="Update contact details">Set Schedule</button>
        <a href="index.php" class="btn btn-info" title="Return back to homepage">Back</a>
    </div>
</form>
<?php include('../layouts/footer.php') ?>
<script>

    const manual_date = document.querySelector("#form_manual_sched");
    const gen_date = document.querySelector("#form_generated_date");
    const btn_manual = document.querySelector("#btn_manual_sched");
    const manual = document.querySelector("#manual");

    btn_manual.addEventListener('click', (e) => {
        e.preventDefault;
        gen_date.style.display = 'none';
        manual_date.style.display = 'block';
        manual.setAttribute('value', 'manual_date');
    })

</script>