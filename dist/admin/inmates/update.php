<?php
// Connect to the database
include_once "../../conn.php";

// Get contact details
if (isset($_GET["id"])) {
    $id = preg_replace('/\D/', '', $_GET["id"]); //Accept numbers only
} else {
    header("Location: indexx.php?p=update&err=no_id");
}

// Update contact details
if (isset($_POST["btnUpdate"])) {
    $name    = $conn->real_escape_string($_POST["txtName"]);
    $email   = $conn->real_escape_string($_POST["txtEmail"]);
    $contact = $conn->real_escape_string($_POST["txtContact"]);
    $is_good_moral = $conn->real_escape_string($_POST["textIsGoodMoral"]);

    if ($stmt = $conn->prepare("UPDATE `inmates` SET `name`=?, `email`=?, `contact`=?, `is_good_moral`=? WHERE `id`=?")) {
        $stmt->bind_param("ssssi", $name, $email, $contact, $is_good_moral, $id);
        $stmt->execute();
        $stmt->close();
        $msg = '<div class="msg msg-update">Contact details updated successfully.</div>';
    } else {
        $msg = '<div class="msg">Prepare() failed: ' . htmlspecialchars($conn->error) . '</div>';
    }
}


if ($stmt = $conn->prepare("SELECT `name`, `email`, `contact`, `is_good_moral` FROM `inmates` WHERE `id`=? LIMIT 1")) {
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->bind_result($name, $email, $contact, $isGoodMoral);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
} else {
    die('prepare() failed: ' . htmlspecialchars($conn->error));
}

// Close database connection
$conn->close();
?>
<?php include('../layouts/header.php') ?>
<?php if (isset($msg)) { ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $msg ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
<div class="title update">
    <h5>Update Contact</h5>
    <hr>
</div>
<form action="<?= $_SERVER['PHP_SELF'] . "?id=" . $id; ?>" method="post" class="frmUpdate">
    <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="txtName" placeholder="Name" value="<?php echo $name; ?>" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input type="email" name="txtEmail" placeholder="Name" value="<?php echo $email; ?>" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Contact</label>
        <input type="number" name="txtContact" placeholder="Name" value="<?php echo $contact; ?>" required class="form-control">
    </div>

    <div class="form-group">
        <label for="">Inmates Good Moral</label>
        <select name="textIsGoodMoral" id="" class="form-control">
            <option value="0" <?php if ($isGoodMoral === 0) {
                                    echo 'selected';
                                } ?>>No</option>
            <option value="1" <?php if ($isGoodMoral === 1) {
                                    echo 'selected';
                                } ?>>Yes</option>

        </select>
    </div>

    <div class="btnWrapper">
        <button type="submit" name="btnUpdate" class="btn btn-primary" title="Update contact details">UPDATE</button>
        <a href="index.php" class="btn btn-info" title="Return back to homepage">BACK</a>
    </div>
</form>
<?php include('../layouts/footer.php') ?>