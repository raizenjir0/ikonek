
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>IKONEK HOMEPAGE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/css/DASHBB.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <!-- <link rel="stylesheet" href="style1234.css"> -->
</head>

<body>

    <input type="checkbox" id="check">

    <header>
        <label for="check">
            <i class="fas fa-bars" id="sidebar_btn"></i>
        </label>
        <div class="left_area">
            <h3>I- <span>KONEK</span></h3>
        </div>
        <div class="right_area">
            <a href="../../destroy_session.php" class="logout_btn">Logout</a>
        </div>
    </header>

    <div class="sidebar">
        <center>
            <img src="../../assets/images/QC logo.png" class="profile_image" alt="">
            <h4>BJMP QC</h4>
        </center>

        <a href="/">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Home Page</span>
        </a>

        <a href="">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>Inmates</span>
        </a>

        <a href="../calendar/calendar/calend.html">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <span>Schedules</span>
        </a>

        <a href="./videocall.html">
            <i class="fa fa-video-camera" aria-hidden="true"></i>
            <span>Video Call</span>
        </a>

        <a href="../gsm.php">
            <i class="fas fa-sms" aria-hidden="true"></i>
            <span>SMS Notifications</span>
        </a>

        <a href="../inmates/report.php">
            <i class="fas fa-info-circle"></i>
            <span>Reports</span>
        </a>


        <!-- <div class="content">