<?php
// session_start();
// if (!isset($_SESSION['username']) && !isset($_SESSION['id'])) {   ?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>I-KONEK LOGIN</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <style>
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
            }

            body {
                height: 100vh;
                width: 100%;
                background-image: url("./assets/images/ikonek1.png");
                background-size: 100% 100%;
                display: flex;
                justify-content: center;
                align-items: center;

            }

            .container {
                width: 450px;
                min-height: 500px;
                background: #FFF;
                border-radius: 3px;
                box-shadow: 0 0 5px rgba(0, 0, 0, .5);
                padding: 40px 30px;
                position: center;
                top: 50%;
            }
        </style>


    </head>

    <body>
        <div class="container">
            <form class="border shadow p-3 rounded" action="php/check-login.php" method="post">
                <h1 class="text-center p-3">LOGIN</h1>
                <?php if (isset($_GET['error'])) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $_GET['error'] ?>
                    </div>
                <?php } ?>
                <div class="mb-3">
                    <label for="username" class="form-label">User name</label>
                    <input type="text" class="form-control" name="username" id="username">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>
                <!-- <div class="mb-1">
                    <label class="form-label">Select User Type:</label>
                </div>
                <select class="form-select mb-3" name="role" aria-label="Default select example">
                    <option selected value="user">User</option>
                    <option value="admin">Admin</option>
                </select> -->

                <button type="submit" class="btn btn-primary">LOGIN</button>
            </form>
        </div>
    </body>

    </html>
<?php 

// } else {
//     header("Location: index.php");
// } 

?>