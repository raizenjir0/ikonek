<?php
	session_start();

	unset($_SESSION['username']);
	unset($_SESSION['role']);
	unset($_SESSION['name']);
	unset($_SESSION['id']);
	session_destroy();
	header('location: index.php');
	// exit();
?>